# %%
import json
import time
import json

from bluepy import btle
import RPi.GPIO as GPIO

mac_adresses = {
    'magenta': "F1:20:CF:62:C6:91",
    'green': "D3:50:ED:01:F1:FA",
    'backup_1' : "C8:77:7E:57:BD:2E",
    'backup_2' : "FC:8A:55:B7:B9:BD"
}

GPIO_for_trigger = 11
time_delay_before_trigger = 1
katapult_start_trigger = 0
trigger_time = 0
spheres_triggered = 0
time_between_sequence_and_catapult = 6
servo_start_time = 1.2 + time_between_sequence_and_catapult
servo_stop_time = servo_start_time + 7.8

def init_GPIO(pin_number):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin_number, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

def wait_for_trigger(pin_number):
    while True:
        GPIO.wait_for_edge(pin_number, GPIO.RISING)
        time.sleep(0.01)
        if(GPIO.input(GPIO_for_trigger) == True):
            break

def check_trigger(pin_number):
    if(GPIO.input(GPIO_for_trigger) == True):
        katapult_start_trigger = 1
        time.sleep(0.01)
        if(GPIO.input(GPIO_for_trigger) == True):
            trigger_time = time.time()
            return 1
    return 0

class MyDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleNotification(self, cHandle):
        print("A notification was received: " )

class sphere():
    def __init__(self, mac_adress, is_backup):
        self.mac_adress = mac_adress
        self.is_backup = is_backup
        self.no_tryouts = 0
        self.max_num_of_tryouts = 2
        
    def connect_and_print_services(self):
        start_time = time.time()
        try:
            self.dev = btle.Peripheral(self.mac_adress)
            self.dev.setDelegate(MyDelegate)
            print("Services...")

            for svc in self.dev.services:
                print(str(svc))

            print(time.time() - start_time)
            sphere_service = self.dev.getServiceByUUID("1101")
            characteristic = sphere_service.getCharacteristics()
            for ch in characteristic:
                print(str(ch))
            trigger_characteristic = characteristic[0]
            self.trigger_characteristic = trigger_characteristic
        except btle.BTLEException as btle_except:
            print("Retrying connection")
            self.connect_and_print_services()
            

    def test_connection(self):
        self.trigger_characteristic.write(b'c', with_Response=True)

    def survived_test(self):
        self.trigger_characteristic.write(b'w', with_Response = True)

    def start_logging(self):
        self.trigger_characteristic.write(b'b')

    def stop_logging(self):
        print("sphere logging stopped")
        self.trigger_characteristic.write(b's')
    
    def trigger_servo(self):
        try:
            self.trigger_characteristic.write(b'w')
        except btle.BTLEException as btle_except:
            print(btle_except)
            if (self.no_tryouts < self.max_num_of_tryouts):
                self.connect_and_print_services()
                self.trigger_characteristic.write(b'w')

    def stop_servo(self):
        try:
            self.trigger_characteristic.write(b'r')
        except btle.BTLEException as btle_except:
            print(btle_except)
            if (self.no_tryouts < self.max_num_of_tryouts + 1):
                self.connect_and_print_services()
                self.trigger_characteristic.write(b'r')

    def sphere_is_backup(self):
        return self.is_backup

def main_loop(time_period):
    print("Waiting for trigger")
    spheres_triggered = 0
    triggered = 0
    servo_stopped = 0

    while True: 
        if(triggered == 0):
            trigger_time = time.time()
            triggered = check_trigger(GPIO_for_trigger)
        else:
            break
    sphere_0.start_logging()
    sphere_1.start_logging()
    start = time.time()
    current_time_diff = time.time() - start
    while(current_time_diff < time_period):
        if(triggered == 0):
            trigger_time = time.time()
            triggered = check_trigger(GPIO_for_trigger)
        
        if((time.time() - trigger_time >= servo_start_time) and spheres_triggered == 0 and triggered == 1):
            print(f"Spheres as backup could be triggered {time.time() - trigger_time}")
            if(sphere_0.sphere_is_backup()):
                sphere_0.trigger_servo()
            if(sphere_1.sphere_is_backup()):
                sphere_1.trigger_servo()
            spheres_triggered = 1
        if((time.time() - trigger_time >= servo_stop_time) and triggered == 1 and servo_stopped == 0):
            print(f"Backup spheres could be stopped {time.time() - trigger_time}")
            if(sphere_0.sphere_is_backup()):
                sphere_0.stop_servo()
            if(sphere_1.sphere_is_backup()):
                sphere_1.stop_servo()
            servo_stopped = 1

        current_time_diff = time.time() - start

#%%
with open('experiment_configuration.json') as f:
  config = json.load(f)
  sphere_0_config = config["sphere_0_config"]
  sphere_1_config = config["sphere_1_config"]

#%%
sphere_0 = sphere(mac_adresses[sphere_0_config["name"]], sphere_0_config["is_backup"])
sphere_1 = sphere(mac_adresses[sphere_1_config["name"]], sphere_1_config["is_backup"])
sphere_0.connect_and_print_services()
sphere_1.connect_and_print_services()

#%%
init_GPIO(GPIO_for_trigger)

main_loop(30)
    
sphere_0.stop_logging()
sphere_1.stop_logging()
